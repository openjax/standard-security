# OpenJAX Security OTP

> Simple API to create and validate _One Time Password_ strings, such as [GAuth][gauth]

[![Build Status](https://travis-ci.org/openjax/security.png)](https://travis-ci.org/openjax/security)
[![Coverage Status](https://coveralls.io/repos/github/openjax/security/badge.svg)](https://coveralls.io/github/openjax/security)
[![Javadocs](https://www.javadoc.io/badge/org.openjax.security/otp.svg)](https://www.javadoc.io/doc/org.openjax.security/otp)
[![Released Version](https://img.shields.io/maven-central/v/org.openjax.security/otp.svg)](https://mvnrepository.com/artifact/org.openjax.security/otp)

### Introduction

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

[gauth]: https://en.wikipedia.org/wiki/Google_Authenticator